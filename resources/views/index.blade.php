@if(Session::has('success'))
    {{ Session::flash('success') }}<br>
@endif

@if (Auth::check())
Logged in!
<a href="{{ url('/auth/logout') }}">Logout</a>
<hr>
{{ dump($data) }}
@else
<a href="{{ url('/auth/login') }}">Login</a>
@endif
