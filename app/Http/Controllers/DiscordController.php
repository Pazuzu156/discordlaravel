<?php

namespace App\Http\Controllers;

use App\Http\Requests\RedirectRequest;
use App\User;
use Auth;
use GuzzleHttp\Client;
use Socialite;

class DiscordController extends Controller
{
    /**
     * Loads the index page.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        // check logged in user
        if (Auth::check()) {
            // new guzzle client, make api call to user's guilds
            $client = new Client();
            $response = $client->get('https://discordapp.com/api/users/@me/guilds', [
                'headers' => [
                    'Authorization' => 'Bearer '.Auth::user()->access_token,
                ],
            ]);

            // return view with data decoded from json response
            $data = json_decode($response->getBody()->getContents());
            return view('index', ['data' => $data]);
        }

        // no logged in user, just return index page
        return view('index');
    }

    /**
     * Redirects the user to Discord to perform Oauth login.
     *
     * @return mixed
     */
    public function getAuthLogin()
    {
        // redirect user to discord with given scopes
        return Socialite::with('discord')->stateless()->scopes(['identify', 'guilds'])->redirect();
    }

    /**
     * Handles redirect callback from Discord.
     *
     * @param \App\Http\Requests\RedirectRequest $request
     *
     * @return Illuminate\View\View
     */
    public function getAuthRedirect(RedirectRequest $request)
    {
        // get socialite user and access token
        $user = Socialite::driver('discord')->stateless()->user();
        $access = $user->accessTokenResponseBody;

        // check if user exists in db
        $userdb = User::where('userid', '=', $user->id);
        if ($userdb->count()) {
            // exists, update access/refresh token with new ones
            $user = $userdb->first();
            $user->access_token = $access['access_token'];
            $user->refresh_token = $access['refresh_token'];

            if ($user->save()) {
                // login user
                Auth::login($user, true);
            }

            return redirect()->to('/');
        }

        // user doesn't exist, create them
        $create = User::create([
            'username' => $user->name,
            'userid' => $user->id,
            'avatar' => $user->avatar,
            'discriminator' => $user->user['discriminator'],
            'email' => $user->email,
            'access_token' => $access['access_token'],
            'refresh_token' => $access['refresh_token'],
        ]);

        if ($create) {
            // login user
            Auth::login($create, true);
            return redirect()->to('/');
        }

        session()->flash('login_error_created', 'Error creating your user! Try again later');
        return redirect()->to('/auth/error');
    }

    /**
     * Fuck if I know... xD
     *
     * @return Illuminate\View\View
     */
    public function getAuthError()
    {
        return view('error');
    }

    /**
     * Log out the user.
     *
     * @return mixed
     */
    public function getAuthLogout()
    {
        // check if user exists (cause we need to remove tokens)
        $user = User::where('userid', '=', Auth::user()->userid);
        if ($user->count()) {
            $user = $user->first();

            // remove tokens
            $user->access_token = '';
            $user->refresh_token = '';

            if ($user->save()) {
                // logout
                Auth::logout();
            }
        }

        return redirect()->to('/');
    }
}
