<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DiscordController@index');
Route::get('/auth/login', 'DiscordController@getAuthLogin');
Route::get('/auth/redirect', 'DiscordController@getAuthRedirect');
Route::get('/auth/error', 'DiscordController@getAuthError');
Route::get('/auth/logout', 'DiscordController@getAuthLogout');
